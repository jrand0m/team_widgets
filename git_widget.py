#!/usr/bin/python
# -*- coding: utf-8 -*-
#TODO: on each event create message with notify-osd or similar 
#tryicon -> [try icon will display how many succesful builds where run of total + color codding]
#           [state icon] project-name ->
#                                       [state icon] build version ->
#                                                                     [failed tests links]
#                                       [other last 5 builds] ...
#                                       [last known good build if exist] 
#                                       block
#                                       restart/stop
#           config
#           exit                           

URL="<url>" #TODO: move to config file
PROJECTS=('<proj-id>',) #TODO: move to config file
JOB_PREFIX='/job/'
API_SUFIX='/api/json'


import sys, json
try:
    from config import URL, PROJECTS
except:
    pass
from PyQt4 import QtGui
from threading import Thread
from httplib import HTTPConnection
def withTimeout(func, args=(), kwargs={}, timeout_duration=10, default=None):
    import threading
    class InterruptableThread(threading.Thread):
        def __init__(self):
            threading.Thread.__init__(self)
            self.result = default
        def run(self):
            self.result = func(*args, **kwargs)
    #log("running func %s with timeout %i " % (str(func), timeout_duration))
    it = InterruptableThread()
    it.start()
    it.join(timeout_duration)
    if it.isAlive():
        #log("exiting alive True func %s" % (str(func)))
        return it.result
    else:
        #log("exiting alive false %s" % (str(func)))
        return it.result

class SystemTrayIcon(QtGui.QSystemTrayIcon):

    def __init__(self, icon, parent=None):
        QtGui.QSystemTrayIcon.__init__(self, icon, parent)
        self.monitor = MonitorManager()
        menu = self.createMenu(parent);
        self.monitor.bindActions(menu)
        self.monitor.startAll()
    
    def createMenu(self, parent):    
        menu = QtGui.QMenu(parent)
        menu.addAction(self.createExitAction())
        self.setContextMenu(menu)
        return menu

    def createExitAction(self):
        exitAction = QtGui.QAction(QtGui.QIcon('exit.png'), '&Exit', self)        
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(self.onExit)
        return exitAction
    def onExit(self):
        self.monitor.stopAll()
        return QtGui.qApp.quit()

#manages threads and binds them to menu items
class MonitorManager():
    
    def __init__(self):
        self.monitors=[]
        #start new tread that will 
        for project in PROJECTS:
            p = Project(project)
            self.createMoniorThread(p)

    def startAll(self):
        for m in self.monitors:
            if not m.isAlive():
                m.setRunnin(True)
                m.start()

    def stopAll(self):
        for m in self.monitors:
            m.setRunnin(False)

    def bindActions(self, menu):
        pass

    def createMoniorThread(self, p):
        m = Monitor(p)
        m.start()
        self.monitors.append(m)
        return m

#this thing is running
class Monitor(Thread):
    def __init__(self, p):
        super(Monitor, self).__init__()
        self.project = p
        self.runnin = True

    def run(self):
        while(self.runnin):
            self.project.fetch()
            import time
            time.sleep(10)

    def setRunnin(self, value):
        self.runnin = value

class Project():
    
    
    def __init__(self,name, display_name=None):
        self.name=name
        self.displayName=display_name
    def fetch(self):
        print("started fetch for %s"%self.name)
        conn = HTTPConnection(URL, timeout=5)#do we need per-instance connection Objects ?
        try:
            #responce = withTimeout(self.conn.request,('GET', JOB_PREFIX+self.name+API_SUFIX,))
            conn.request('GET', JOB_PREFIX+self.name+API_SUFIX)
            resp = conn.getresponse().read().decode('utf-8')
            print("success fetch for %s"%self.name)
            resp = json.loads(resp)
            print('resp ->' + str(resp['builds'][0]['number']))
        except Error as e:
            print ("error -> " + str(e))
        print("ended fetch for %s"%self.name)
        conn.close()

def main():
    app = QtGui.QApplication(sys.argv)

    w = QtGui.QWidget()
    trayIcon = SystemTrayIcon(QtGui.QIcon("git.png"), w)

    trayIcon.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
